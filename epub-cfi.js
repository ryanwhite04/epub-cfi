function showNodeFromCFI(iframe, epubcfi, isbn) {
    getMeta(isbn, function(meta) {
        getOpf(meta, function(opf) {
            var href = getHref(opf, epub-cfi, callback);
            iframe.onload = function() {
                var epubDoc = iframe.contentDocument;
                var node = getNode(epubDoc, epub-cfi);
                node.scrollIntoView();
            };
            iframe.src = href;
        });
    });
}

// get the xml of the container.xml file in META-INF dir, and feed to callback
function getMeta(isbn, callback) {
    $.get('../book/' + isbn + '/META-INF/container.xml', function(meta) {
        callback(meta);
    }, 'xml');

}

// get the xml of the content.opf file and feed to callback
function getOpf(meta, callback) {
    var href = meta.getElementsByTagName('rootfile')[0].getAttribute('full-path');
    $.get('../book/' + isbn + '/' + href, function(opf) {
        callback(opf);
    }, 'xml');
}

// get the href of the html file from the opfXML
function getHref(opf, epub-cfi) {
    var href;
    // TODO
    return href;
}

// get the node specified by the epub-cfi in the given document
function getNode(document, epub-cfi) {
    var id = getId(epub-cfi);
    var node = document.getElementById(id);
    return node;
}
